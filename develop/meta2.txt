#cloud-config
users:
  - name: ubuntu
    groups: sudo
    shell: /bin/bash
    sudo: ['ALL=(ALL) NOPASSWD:ALL']
    ssh-authorized-keys:
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDDEOWWPQod6zXfJDjrNupEZXIDBcHeJ32Nc12CjORlxinEPx0rhCSyO9xj6O3I9vEISiIWO7G+Nz66TnAVQcXLtP2pf3bxsr2ED06BxjUAv2ZJ7F+1jj1ig1Ig/E2J7BxCrwN05C7EMX+SCp01CAOB8D2AHPrgRm4oQiP/XJKAdbM7TpJOQBrYoeyHTqnZw7PMz9trwWexv+FARvCEFmAWOpkwyBHZaMkyEFszEfu646Em5Cyy4/MHlpVOXW1f3nIPJL9pk5WwWEOufsFbMHxKPwgRzBitqRx3gEJejZabzeVWUsVU1eZJNuDUqT7vM8uX8q0qKyPLsLTOocedog2D
