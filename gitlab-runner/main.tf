##### Variables #####

locals {
  local_data = jsondecode(file("${path.module}/config.json"))
}

##### General #####

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = local.local_data.KEY_FILE
  cloud_id                 = local.local_data.CLOUD_ID
  folder_id                = local.local_data.FOLDER_ID
  zone                     = local.local_data.ZONE
}

##### Net & Subnet definition #####

resource "yandex_vpc_network" "network-2" {
  name = "network-2"
}

resource "yandex_vpc_subnet" "subnet-2" {
  name           = "subnet-2"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-2.id
  v4_cidr_blocks = ["192.168.2.0/24"]
}

