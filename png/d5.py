from diagrams import Cluster, Diagram, Edge
from diagrams.aws.network import ELB
from diagrams.onprem.monitoring import Grafana, Prometheus
from diagrams.onprem.network import Nginx
from diagrams.onprem.client import Client
from diagrams.gcp.analytics import Dataflow
from diagrams.onprem.ci import GitlabCI
from diagrams import Cluster, Diagram
from diagrams.aws.compute import ECS
from diagrams.aws.database import ElastiCache, RDS
from diagrams.aws.network import Route53
from diagrams.elastic.elasticsearch import ElasticSearch, Logstash
from diagrams.programming.language import Go
from diagrams.onprem.logging import Rsyslog, Loki
from diagrams.onprem.network import Consul


with Diagram("SkillBox Project Advanced", show=False):
    gitlab = GitlabCI("gitlab.com")
    with Cluster("Local VM"):
        with Cluster("Docker"):
            with Cluster("Gitlab Runner"):
                runner = GitlabCI("Local Runner")


    
    with Cluster("Yandex Cloud") as project:
        with Cluster("VM Monitoring"):
#            with Cluster("Docker"):
#                with Cluster("Gitlab Runner"):
#                    runner = GitlabCI("Gitlab Runner")


            primary = Prometheus("Prometheus")
            secondary = Grafana("Grafana")
            tertiary = Consul("Consul")
            primary - secondary - tertiary
            

        with Cluster("VM1"):
            with Cluster("Docker"):
                with Cluster("Service"):
                    service1 = Go("Skillboxapp")
#            rsyslog = Rsyslog("Logging")

        with Cluster("VM2"):
            with Cluster("Docker"):
                with Cluster("Service"):
                    service2 = Go("Skillboxapp")

        with Cluster("VM3"):
            with Cluster("Docker"):
                with Cluster("Service"):
                    service3 = Go("Skillboxapp")


        elb = ELB("Yandex App Load Balancer")
#        tertiary << rsyslog
        primary >> elb >> service1 >> tertiary
        elb >> service2 >> tertiary
        elb >> service3 >> tertiary
    
    gitlab >> runner >> project
#    monitoring >> service
#    client >> monitoring
